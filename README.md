# Front End Test For Prime Creative

Features:
 - SASS
 - Bootstrap 4
 - Image optimization
 - Dev server
 - HMR

## Explanation

I firstly created a quick boilerplate for a simple landing page here (I sourced a couple of online projects and edited to suite my needs) - https://gitlab.com/Naphoria/landing-page-boilerplate

I then cloned the boilerplate, created another repo for the front end test, and began development - https://gitlab.com/Naphoria/prime-front-end

If you check the branches section, I tend to create a whole new branch for each feature. This stops any conflict when multiple developers are working together or if multiple features are added at once.

I started from mobile and worked towards desktop, I know you didn’t specify but in a real world scenario it would be pointless to just create the desktop version.

With it being one page like a landing page, I made it straight into a HTML document, if this was a normal website I would have all my sections in partials.

The font from the PSD was a paid one that I didn’t have access to so I used the closest one I could find from google fonts.

The only confusion I had was with the parallax. There wasn't any content to scroll towards, I just added some extra padding so you can see the parallax effect.

Hope this is all okay, It was quite nice to test my skills even though it was straight forward.

## To View

1. Clone or download project
2. cd into prime-front-end
3. run `npm install`
4. For dev server run `npm run start`
5. For production files run `npm run build`

Production files are generated into the dist folder (Use for browser viewing purposes)

Development files are inside src folder (Use to see how it was built)