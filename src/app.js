import './scss/style.scss';
import Swiper from '../node_modules/swiper/swiper-bundle.js';
var WebFont = require("webfontloader");

import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'


// Load font asynchronously
WebFont.load({
    google: { families: ["Open+Sans:400,700"] }
});

// Swiper slider used for handling the carousel
var swiper = new Swiper('.swiper-testimonials', {
    spaceBetween: 30,
    // centeredSlides: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: true,
    },
    navigation: {
      nextEl: '#chevron-swiper-arrow-right',
      prevEl: '#chevron-swiper-arrow-left',
    },
  });